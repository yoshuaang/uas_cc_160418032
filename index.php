<?php
$mysqli = new mysqli("localhost", "root", "", "uas");
if ($mysqli->connect_error) 
{
    die("Connection failed: " . $mysqli->connect_error);
}

header("Access-Control-Allow-Origin: *");
header("Cache-Control: no-cache");
header("Access-Control-Expose-Headers", "Access-Control-*");
header("Access-Control-Allow-Headers", "Access-Control-*, Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS, HEAD');
header('Allow', 'GET, POST, PUT, DELETE, OPTIONS, HEAD');

$sql = "select * from data";
$stmt = $mysqli->prepare($sql);
$stmt->execute();
$res = $stmt->get_result();

if ($res->num_rows > 0)
{
	$data = array();

    while ($obj = $res->fetch_assoc())
    {
        $data['id'] = $obj['id'];
        $data['nama'] = $obj['nama'];
        $data['nrp'] = $obj['nrp'];
        $data['alamat'] = $obj['alamat'];
        $data['kota'] = $obj['kota'];
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Percobaan UAS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/my-css.css">
</head>
<body>
	<!-- NAVBAR -->
	<nav class="navbar navbar-expand-md justify-content-center">
		<div class="container-fluid">
		  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#wrap" aria-controls="wrap" aria-expanded="false" aria-label="Toggle navigation">
				<i class="fa fa-ellipsis-v" aria-hidden="true"></i>
			</button>
		  	<div class="collapse navbar-collapse" id="wrap">
				<div class="wrapper">
					<div class="button active-btn">
						<div class="icon">
							<i class="fa fa-home" aria-hidden="true"></i>
						</div>
						<span>Home</span>
					</div>
					<div class="button">
						<div class="icon">
							<i class="fa fa-user" aria-hidden="true"></i>
						</div>
						<span>About</span>
					</div>
					<div class="button">
						<div class="icon">
							<i class="fa fa-file" aria-hidden="true"></i>
						</div>
						<span>Resume</span>
					</div>
					<div class="button">
						<div class="icon">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</div>
						<span>Contact</span>
					</div>
				</div>  
			</div>		  
		</div>
	</nav>

	<!-- HOME -->
	<section id="my-home" class="d-flex flex-column justify-content-center">
		<div class="container home">
			<p>Hi There, I`m</p>
            <?php 
                echo "<h1>$data[nama] - $data[nrp]</h1>";
                echo "<h1>$data[alamat] - $data[kota]</h1>";
            ?>
		</div>
	</section>	
</body>
	<script src="js/jquery/jquery.slim.min.js"></script>
	<script src="js/popper/umd/popper.min.js"></script>
	<script src="js/bootstrap/bootstrap.min.js"></script>
	<script src="js/my-jquery.js"></script>
</html>